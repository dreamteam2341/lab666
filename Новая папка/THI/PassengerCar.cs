﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THI
{
    public class PassengerCar:Car
    {
        private string _carNumber = "";

        public string CarNumber
        {
            get { return _carNumber; }
            set { _carNumber = value; }
        }

        public override int CalcPrice()
        {
            return BasicPrice;
        }
    }
}
