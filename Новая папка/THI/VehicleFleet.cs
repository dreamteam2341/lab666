﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THI
{
    public class VehicleFleet
    {
        private string _name;
        private List<Car> _carsInFleet = new List<Car>();
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public List<Car> CarsInFleet
        {
            get { return _carsInFleet; }
        }

        public void AddCar(Car car)
        {
            CarsInFleet.Add(car);
        }

        public void RemoveCar(Car car)
        {
            CarsInFleet.Remove(car);
        }
    }
}
