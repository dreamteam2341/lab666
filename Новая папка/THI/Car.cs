﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THI
{
    public class Car
    {
        private int _basicPrice;
        private int _enginePower;
        private string _name = "";

        public int BasicPrice
        {
            get { return _basicPrice; }
            set { _basicPrice = value; }
        }
        public int EnginePower
        {
            get { return _enginePower; }
            set { _enginePower = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public virtual int CalcPrice()
        {
            return BasicPrice;
        }

        public override String ToString()
        {
            return Name;
        }
    }
}
