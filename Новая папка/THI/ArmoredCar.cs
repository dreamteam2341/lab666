﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THI
{
    public class ArmoredCar:PassengerCar
    {
        private int _armorLevel;
        private const int _priceCoef = 200;

        public int ArmorLevel
        {
            get { return _armorLevel; }
            set { _armorLevel = value; }
        }

        public override int CalcPrice()
        {
            return BasicPrice + _priceCoef * ArmorLevel;
        }
    }
}
