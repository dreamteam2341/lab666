﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THI
{
    public class RacingCar:Car
    {
        private int _numberOfAwards;
        private const int _priceCoef = 1000;

        public int NumberOfAwards
        {
            get { return _numberOfAwards; }
            set { _numberOfAwards = value; }
        }

        public override int CalcPrice()
        {
            return BasicPrice + _priceCoef * NumberOfAwards;
        }
    }
}
