﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THI;

namespace Logic
{
    public class FleetPrinter
    {
        public static void Print(VehicleFleet vehicleFleet)
        {
            Console.WriteLine("===================================================");
            Console.WriteLine("Автопарк <<"+vehicleFleet.Name+">>");
            Console.WriteLine("===================================================");
            List<Car> cars = vehicleFleet.CarsInFleet;
            Console.WriteLine("Кол-во автомобилей: " + cars.Count);
            Console.WriteLine("___________________________________________________");
            foreach(Car car in cars)
            {
                Console.WriteLine("{2,-15} - {0,-20} - {1,5}",car.Name,car.CalcPrice(),car.GetType().Name);
            }
            Console.WriteLine("___________________________________________________");
        }
    }
}
