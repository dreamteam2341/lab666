﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public static class LoggerFactory
    {
        public enum LoggerType
        {
            FileLogger, ConsoleLogger
        }
        public static ILogger CreateLogger(LoggerType type)
        {
            ILogger logger;
            switch (type)
            {
                case LoggerType.FileLogger:
                    logger = new FileLogger();
                    break;
                case LoggerType.ConsoleLogger:
                    logger = new ConsoleLogger();
                    break;
                default:
                    logger = null;
                    break;
            }
            return logger;
        }
    }
}
