﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    class FileLogger : ILogger
    {
        private string _path;
        public FileLogger()
        {
            _path = @"logs/log_" + DateTime.Now.ToShortDateString().Replace(".", "") + " " + DateTime.Now.ToShortTimeString().Replace(":", "") + ".txt";
            Directory.CreateDirectory(@"logs");
            File.Create(_path).Close();
        }
        public void Log(string message)
        {
            using (StreamWriter sw = new StreamWriter(_path, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " | " + message);
            }
        }
    }
}
