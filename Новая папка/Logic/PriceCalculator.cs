﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THI;

namespace Logic
{
    public class PriceCalculator
    {
        public static int GetTotalPrice(VehicleFleet vehicleFleet)
        {
            int totalPrice = 0;
            foreach (Car car in vehicleFleet.CarsInFleet)
            {
                totalPrice += car.CalcPrice();
                if (totalPrice < 0)
                    throw new Exception();
            }
            return totalPrice;
        }
    }
}
