﻿using THI;
   
namespace Logic
{
    public class FleetFactory
    {
        public static VehicleFleet CreateFleet()
        {
            VehicleFleet fleet = new VehicleFleet();
            fleet.Name = "TPU fleet";

            PassengerCar toyoutaCorolla = new PassengerCar();
            toyoutaCorolla.Name = "Toyouta Corolla";
            toyoutaCorolla.BasicPrice = 300000;
            toyoutaCorolla.CarNumber = "A234TO";
            toyoutaCorolla.EnginePower = 130;

            ArmoredCar armoredToyoutaCorolla = new ArmoredCar();
            armoredToyoutaCorolla.Name = "Toyouta Corolla (a)";
            armoredToyoutaCorolla.BasicPrice = 300000;
            armoredToyoutaCorolla.CarNumber = "A234TO";
            armoredToyoutaCorolla.EnginePower = 130;
            armoredToyoutaCorolla.ArmorLevel = 270;

            RacingCar toyoutaSupra = new RacingCar();
            toyoutaSupra.BasicPrice = 2000000;
            toyoutaSupra.EnginePower = 500;
            toyoutaSupra.Name = "Toyouta Supra";
            toyoutaSupra.NumberOfAwards = 10;

            fleet.AddCar(toyoutaCorolla);
            fleet.AddCar(armoredToyoutaCorolla);
            fleet.AddCar(toyoutaSupra);

            return fleet;
        }
    }
}