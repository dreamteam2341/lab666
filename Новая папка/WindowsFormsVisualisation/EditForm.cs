﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using THI;

namespace WindowsFormsVisualisation
{
    public partial class EditForm : Form
    {
        private DialogResult _result;
        /// <summary>
        /// Ссылка на машину, которую хотели отредактировать
        /// </summary>
        private Car _carForInitFields;

        private MainForm _parentForm;
        public Car CarForInitFields
        {
            get { return _carForInitFields; }
            set { _carForInitFields = value; }
        }
        public EditForm(MainForm mainForm):this(mainForm, null) { }
        public EditForm(MainForm mainForm, Car carForInitFields)
        { 
            InitializeComponent();

            _result = DialogResult.None;
            _parentForm = mainForm;
            CarForInitFields = carForInitFields;
            addElementsToComboBox();
            
            
            if (carForInitFields != null)
            {
                Text = "Edit " + carForInitFields.ToString();
                selectIndex(carForInitFields);
            }
            else
            {
                Text = "Add new";
                carClassComboBox.SelectedIndex = 0;
            }
        }
        /// <summary>
        /// Задаёт список классов машин, которые могут быть в автопарке.
        /// </summary>
        private void addElementsToComboBox()
        {
            foreach(var o in ClassRegistrator.RegisterClasses())
            {
                carClassComboBox.Items.Add(o);
            }
        }
        /// <summary>
        /// Выбирает в ComboBox элемент, соответствующий заданному классу car.
        /// </summary>
        /// <param name="car"></param>
        private void selectIndex(Car car)
        {
            var itemsInComboBox = carClassComboBox.Items;
            Type carType = car.GetType();
            carClassComboBox.SelectedIndex = itemsInComboBox.IndexOf(carType);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            _result = DialogResult.OK;
            try
            {
                GetCar();
            }
            catch (Exception)
            {
                _result = DialogResult.None;
                MessageBox.Show("Не все поля заполнены корректно", "ERROR");
                return;
            }
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            _result = DialogResult.Cancel;

            Close();
        }
        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        } 

        private void EditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _parentForm.OnModalWindowClose(this);
        }
        /// <summary>
        /// Создаёт машину на основе заполненных полей
        /// </summary>
        /// <returns></returns>
        public Car GetCar()
        {
            Type type = carClassComboBox.SelectedItem as Type;
            if (type == null)
            {
                MessageBox.Show("Не удалось получить тип, выбранный в ComboBox", "ERROR");
                throw new Exception();
            }
            Car car = CreateEmptyCar(type);
            if (car == null)
            {
                MessageBox.Show("Не удалось вызвать конструктор для выбранного типа", "ERROR");
                throw new Exception();
            }
            DefaultDataPanelFiller.CreateObject(car, dataPanel);
            return car;
        }
        /// <summary>
        /// Вызывает пустой конструктор класса type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static Car CreateEmptyCar(Type type)
        {
            var emptyConstructor = type.GetConstructor(new Type[] { });
            Car car = emptyConstructor.Invoke(new Object[] { }) as Car;
            return car;
        }

        private void carClassComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Type type = carClassComboBox.SelectedItem as Type;
            if (type == null)
            {
                MessageBox.Show("Не удалось получить тип, выбранный в ComboBox", "ERROR");
                return;
            }
            Car car = CreateEmptyCar(type);
            if (car == null)
            {
                MessageBox.Show("Не удалось вызвать конструктор для выбранного типа", "ERROR");
                return;
            }
            FillDataPanelWithControls(car);
            if (CarForInitFields!=null)
            {
                FillSimilarControls(CarForInitFields);
            }
        }
        /// <summary>
        /// Добавляет на форму элементы TextBox для всех полей объекта car
        /// </summary>
        /// <param name="car">Экземпляр класса, которому нужно заполнить поля</param>
        private void FillDataPanelWithControls(Car car)
        {
            DefaultDataPanelFiller.FillPanel(dataPanel, car);
        }
        /// <summary>
        /// Заполняет элементы TextBox информацией из объекта car
        /// </summary>
        /// <param name="car"></param>
        private void FillSimilarControls(Car car)
        {
            DefaultDataPanelFiller.SincPanelWithObject(car, dataPanel);
        }

    }
}
