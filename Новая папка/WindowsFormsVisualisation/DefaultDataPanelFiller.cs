﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using THI;

namespace WindowsFormsVisualisation
{
    public static class DefaultDataPanelFiller
    {
        /// <summary>
        /// Очищает панель и помещает на неё элементы управления для редактирования полей объекта object.
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="obj"></param>
        public static void FillPanel(FlowLayoutPanel panel, object obj)
        {
            
            panel.Controls.Clear();
            var fields = TypeDescriptor.GetProperties(obj);
            foreach(PropertyDescriptor field in fields)
            {
                string fieldName = field.DisplayName;
                Label fieldNameLabel = new Label();
                fieldNameLabel.Text = field.DisplayName;

                TextBox fieldTextBox = new TextBox();
                fieldTextBox.Name = fieldName;
                if (typeof(int).Equals(field.PropertyType))
                {
                    fieldTextBox.TextChanged += new EventHandler(OnTextChangedWhenIntRequired);
                    fieldTextBox.HandleCreated += new EventHandler(OnTextChangedWhenIntRequired);
                }
                //
                // DANGER
                //
                if (fieldName.Equals("CarNumber")) // Привязка к имени переменной. Опасно!
                {
                    fieldTextBox.TextChanged += new EventHandler(OnTextChangedWhenCarNumberRequired);
                    fieldTextBox.HandleCreated += new EventHandler(OnTextChangedWhenCarNumberRequired);
                }
                else if (typeof(string).Equals(field.PropertyType))
                {
                    fieldTextBox.TextChanged += new EventHandler(OnTextChangedWhenStringRequired);
                    fieldTextBox.HandleCreated += new EventHandler(OnTextChangedWhenStringRequired);
                }

                TableLayoutPanel fieldPanel = new TableLayoutPanel();
                fieldPanel.ColumnCount = 2;
                fieldPanel.RowCount = 2;
                fieldPanel.AutoSize = true;
                fieldPanel.Controls.Add(fieldNameLabel,0,0);
                fieldPanel.Controls.Add(fieldTextBox,1,0);
                Label errorMessage = new Label();
                errorMessage.ForeColor = System.Drawing.Color.Red;
                errorMessage.Name = "error";
                fieldPanel.Controls.Add(errorMessage, 1, 1);
                panel.Controls.Add(fieldPanel);
            }
        }
        /// <summary>
        /// Заполняет поля объекту obj. Информация берётся из элементов управления на панели dataPanel
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="dataPanel"></param>
        public static void CreateObject(object obj, Panel dataPanel)
        {
            var fields = TypeDescriptor.GetProperties(obj);
            foreach (PropertyDescriptor field in fields)
            {
                string fieldName = field.DisplayName;
                Control[] controls = dataPanel.Controls.Find(fieldName, true);
                if (controls.Count() != 0)
                {
                    TextBox textBox = controls[0] as TextBox;
                    if (textBox != null)
                    {
                        String value = textBox.Text;
                        if (value.Equals(""))
                        {
                            throw new Exception();
                        }
                        if (typeof(string).Equals(field.PropertyType))
                        {
                            field.SetValue(obj, value);
                        }
                        if (typeof(int).Equals(field.PropertyType))
                        {
                            field.SetValue(obj, Convert.ToInt32(value));
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Заполняет элементы управления панели значениями полей объекта obj.
        /// </summary>
        /// <param name="car"></param>
        /// <param name="dataPanel"></param>
        public static void SincPanelWithObject(object obj, Panel dataPanel)
        {
            var fields = TypeDescriptor.GetProperties(obj);
            foreach (PropertyDescriptor field in fields)
            {
                string fieldName = field.DisplayName;
                Control[] controls = dataPanel.Controls.Find(fieldName, true);
                foreach (Control control in controls)
                {
                    TextBox textBox = control as TextBox;
                    if (textBox != null)
                    {
                        String textBoxName = textBox.Name;
                        if (textBoxName.Equals(fieldName))
                        {
                            string val = obj.GetType().GetProperty(fieldName).GetValue(obj).ToString();
                            textBox.Text = val;
                        }
                    }
                }
            }
        }
        private static void OnTextChangedWhenStringRequired(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox != null)
            {
                Label error = textBox.Parent.Controls.Find("error",false)[0] as Label;
                textBox.BackColor = System.Drawing.Color.White;
                error.Text = "";
                if (textBox.Text.Equals(""))
                {
                    textBox.BackColor = System.Drawing.Color.LightPink;
                    error.Text="Введите строку";
                }
            }
        }
        private static void OnTextChangedWhenCarNumberRequired(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox != null)
            {
                Label error = textBox.Parent.Controls.Find("error", false)[0] as Label;
                textBox.BackColor = System.Drawing.Color.White;
                error.Text = "";
                String text = textBox.Text;
                if (text.Length!=6)
                {
                    textBox.BackColor = System.Drawing.Color.LightPink;
                    error.Text = "БЦБЦББ";
                }
                else
                {
                    if (!(Char.IsLetter(text[0]) && Char.IsDigit(text[1]) && Char.IsDigit(text[2]) && Char.IsDigit(text[3])&&
                        Char.IsLetter(text[4]) && Char.IsLetter(text[5])))
                    {
                        textBox.BackColor = System.Drawing.Color.LightPink;
                        error.Text = "БЦЦЦББ";
                    }
                }
            }
        }
        private static void OnTextChangedWhenIntRequired(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox != null)
            {
                Label error = textBox.Parent.Controls.Find("error", false)[0] as Label;
                textBox.BackColor = System.Drawing.Color.White;
                error.Text = "";
                try
                {
                    Convert.ToInt32(textBox.Text);
                }
                catch (Exception)
                {
                    bool tooLong = true;
                    try
                    {
                        BigInteger b = 0;
                        tooLong = BigInteger.TryParse(textBox.Text,out b);
                    }
                    catch (Exception)
                    {
                        tooLong = false; // Не получилось сделать длинное число
                    }
                    if (tooLong == true)
                    {
                        textBox.BackColor = System.Drawing.Color.LightPink;
                        error.Text = "Надо меньше";
                    }
                    else
                    {
                        textBox.BackColor = System.Drawing.Color.LightPink;
                        error.Text = "Это не число";
                    }
                    return;
                }
              ///  if (Convert.ToInt32(textBox.Text) < 0)
              ///  {
              ///      textBox.BackColor = System.Drawing.Color.LightPink;
              ///      error.Text = "Число отриц.";
              ///  }
            }
        }
    }
}
