﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using THI;
using Logic;

namespace WindowsFormsVisualisation
{
    public partial class MainForm : Form
    {
        private VehicleFleet _vehicleFleet;
        List<EditForm> openedEditForms = new List<EditForm>();
        private const int MAXOPENEDEDITFORMSCOUNT = 3;
        public VehicleFleet VehicleFleet
        {
            get
            {
                return _vehicleFleet;
            }

            set
            {
                _vehicleFleet = value;
            }
        }

        public MainForm()
        {
            InitializeComponent();
            VehicleFleet = FleetFactory.CreateFleet();
            foreach (Car car in VehicleFleet.CarsInFleet)
            {
                this.carsInFleetListBox.Items.Add(car);
            }
            updateButtonLocks();
            updateTotalPrice();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (openedEditForms.Count == MAXOPENEDEDITFORMSCOUNT)
            {
                MessageBox.Show("Достигнуто максимальное кол-во открытых окон","ERROR");
                return;
            }
            EditForm editForm = new EditForm(this);
            editForm.Show(this);
            openedEditForms.Add(editForm);
        }
        public void OnModalWindowClose(EditForm editForm)
        {
            openedEditForms.Remove(editForm);
            if (editForm.Result == DialogResult.OK)
            {
                Car carToBeEdited = editForm.CarForInitFields;
                Car newCar = editForm.GetCar();
                if (editForm.CarForInitFields == null)
                {
                    this.carsInFleetListBox.Items.Add(newCar);
                    VehicleFleet.AddCar(newCar);
                }
                else
                {
                    this.carsInFleetListBox.Items.Remove(carToBeEdited);
                    this.carsInFleetListBox.Items.Add(newCar);
                    VehicleFleet.RemoveCar(carToBeEdited);
                    VehicleFleet.AddCar(newCar);
                }
                updateButtonLocks();
                updateTotalPrice();
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            Car carToBeEdited = carsInFleetListBox.SelectedItem as Car;
            if (carToBeEdited == null)
            {
                MessageBox.Show("Не выбран элемент для редактирования либо выбранный элемент не принадлежит классу Car.", "ERROR");
                return;
            }
            if (FindEditFormByEditedCar(carToBeEdited) != null)
            {
                ShowEditFormByCar(carToBeEdited);
            }
            else {
                if (openedEditForms.Count == MAXOPENEDEDITFORMSCOUNT)
                {
                    MessageBox.Show("Достигнуто максимальное кол-во открытых окон", "ERROR");
                    return;
                }
                EditForm editForm = new EditForm(this, carToBeEdited);
                editForm.Show(this);
                openedEditForms.Add(editForm);
            }
        }

        private void ShowEditFormByCar(Car carToBeEdited)
        {
            var existingForm = FindEditFormByEditedCar(carToBeEdited);
            existingForm.WindowState = FormWindowState.Normal;
            existingForm.Activate();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Car carToBeDeleted = carsInFleetListBox.SelectedItem as Car;
            if (carToBeDeleted==null)
            {
                MessageBox.Show("Не выбран элемент для удаления либо выбранный элемент не принадлежит классу Car.", "ERROR");
                return;
            }
            if (FindEditFormByEditedCar(carToBeDeleted) != null)
            {
                MessageBox.Show("Сначала закройте окно редактирования для выбранной машины.", "ERROR");
                ShowEditFormByCar(carToBeDeleted);
                return;
            }
            VehicleFleet.RemoveCar(carToBeDeleted);
            carsInFleetListBox.Items.Remove(carToBeDeleted);
            updateButtonLocks();
            updateTotalPrice();
        }
        private void updateButtonLocks()
        {
            Car selectedCar = carsInFleetListBox.SelectedItem as Car;
            if (selectedCar == null)
            {
                editButton.Enabled = false;
                deleteButton.Enabled = false;
            }
            else
            {
                editButton.Enabled = true;
                deleteButton.Enabled = true;
            }
        }

        private void carsInFleetListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateButtonLocks();
        }
        private void updateTotalPrice()
        {
            try {
                totalPriceLabel.Text = "Total price: " + PriceCalculator.GetTotalPrice(VehicleFleet).ToString();
            }
            catch
            {
                totalPriceLabel.Text = "Total price: infinity";
                MessageBox.Show("Переполнение стоимости автопарка","ERROR");
            }
        }
        private EditForm FindEditFormByEditedCar(Car car)
        {
            if (car == null)
                return null;
            foreach(var form in openedEditForms)
            {
                if (car.Equals(form.CarForInitFields))
                {
                    return form;
                }
            }
            return null;
        }

        private void carsInFleetListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            editButton_Click(sender, e);
        }
    }
}