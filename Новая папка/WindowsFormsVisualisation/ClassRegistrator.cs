﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THI;

namespace WindowsFormsVisualisation
{
    public static class ClassRegistrator
    {
        public static Type[] RegisterClasses()
        {
            List<object> result = new List<object>();
            result.Add((new PassengerCar()).GetType());
            result.Add((new ArmoredCar()).GetType());
            result.Add((new RacingCar()).GetType());
            return Array.ConvertAll(result.ToArray(), item => (Type)item);
        }
    }
}
