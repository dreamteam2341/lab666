﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;
using THI;

namespace ConsoleVisualisation
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger logger = LoggerFactory.CreateLogger(LoggerFactory.LoggerType.FileLogger);
            logger.Log("Программа запущена");
            VehicleFleet vehicleFleet = FleetFactory.CreateFleet();
            logger.Log("Автопарк создан");
            FleetPrinter.Print(vehicleFleet);
            logger.Log("Автопарк напечатан");
            int price = PriceCalculator.GetTotalPrice(vehicleFleet);
            logger.Log("Посчитана стоимость автопарка");
            logger.Log("Запущен нагрузочный цикл");
            int x = 2;
            for(long  i = 0; i < 50000000; i++)
            {
                int a = 2;
                long b = a + i * i-x;
                x = (int)Math.Sqrt(b);
            }
            x--;
            logger.Log("Нагрузочный цикл завершён");
            Console.WriteLine("Общая цена: " + price);
            logger.Log("Выведена стоимость автопарка");
            Console.ReadKey();
        }
    }
}
